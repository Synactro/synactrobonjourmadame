# Build

## Build base image

```
cd docker/image/base && ./build.sh && cd ../../../
```

## Build & Push final image (need base)

### Bulid only final image

```
cd docker/image/final && ./build.sh && cd ../../../
```

### Bulid & push final image into repository for prod (BE CAREFULL!!!)

```
cd docker/image/final && ./build.sh && ./push.sh && cd ../../../
```

## Build dev image (need base)

```
cd docker/image/dev && ./build.sh && cd ../../../
```

# Start

## Start for dev

### Edit file: docker/env.sh

Choose a free port:

```
export PORT_APACHE="89"
```

```
cd docker/ && ./start.sh && cd ../
```

## Start final image for test locally

### Edit file: docker/image/final/env.sh

Choose a free port:

```
export PORT_APACHE="89"
```

```
cd docker/image/final && ./start.sh && cd ../../../
```

# Stop

## Stop for dev

```
cd docker/ && ./stop.sh && cd ../
```

## Stop final image

```
cd docker/image/final && ./stop.sh && cd ../../../
```