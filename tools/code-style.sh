#!/usr/bin/env bash

find ../ -type f -name '.directory' | xargs rm -rf

./php-cs-fixer fix ../src/src --rules=@Symfony,@PhpCsFixer