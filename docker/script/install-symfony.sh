#!/usr/bin/env bash

source ../env.sh

CMD_DOCKER="docker exec -ti --user user ${IMAGE_NAME}"

${CMD_DOCKER} composer create-project symfony/skeleton /var/www

${CMD_DOCKER} composer require annotations
${CMD_DOCKER} composer require guzzlehttp/guzzle:~6.0
${CMD_DOCKER} composer require symfony/dom-crawler
${CMD_DOCKER} composer require symfony/polyfill-php72
${CMD_DOCKER} composer require symfony/dom-crawler
${CMD_DOCKER} composer require symfony/cache
