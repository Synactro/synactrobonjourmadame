<?php

namespace App\Controller;

use GuzzleHttp\Client;
use Psr\SimpleCache\CacheInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

class HomeController
{
	/**
	 * @Route("/")
	 */
	public function home(Client $client, CacheInterface $cache): Response
	{
		$img = $this->getImageFromBonjourMadame($client, $cache);

		$response = new Response();
		$disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_INLINE, 'bonjourmadame.jpeg');
		$response->headers->set('Content-Disposition', $disposition);
		$response->headers->set('Content-Type', 'image/jpeg');
		$response->setContent($img);

		return $response;
	}

	/**
	 * @Route("/json")
	 */
	public function json(Client $client, CacheInterface $cache): Response
	{
		$url = $cache->get('url');

		if (isset($url)) {
			return new JsonResponse(['url' => $url]);
		}

		$response = $client->request('GET', 'http://www.bonjourmadame.fr/');
		$html = (string) $response->getBody();

		$crawler = new Crawler($html);
		$uriImage = $crawler->filterXPath('//div[@class="post-content"]/p//img')->image()->getUri();

		$response = $client->request('GET', 'https://images.6emesens.org?json='.urlencode($uriImage));

		$arr = json_decode((string) $response->getBody(), true);

		$dateTime = new \DateTime();
		$day = $dateTime->format('Y-m-d');

		if (isset($arr['url'])) {
			$url = $arr['url'].'bonjour-madame-'.$day.'.jpg';
			$cache->set('url', $url, 10 * 60);

			return new JsonResponse(['url' => $url]);
		}

		return new JsonResponse(['error' => 'not found']);
	}

	/**
	 * @Route("/ip")
	 */
	public function ip(Client $client): Response
	{
		$response = $client->request('GET', 'https://mon-ip.io/');
		$html = (string) $response->getBody();

		$crawler = new Crawler($html);
		$ip = $crawler->filterXPath('//p[contains(@id, "ip")]')->text();

		return new Response($ip);
	}

	private function getImageFromBonjourMadame(Client $client, CacheInterface $cache): string
	{
		$img = $cache->get('img');

		if (isset($img)) {
			return base64_decode($img);
		}

		$response = $client->request('GET', 'http://www.bonjourmadame.fr/');
		$html = (string) $response->getBody();

		$crawler = new Crawler($html);

		$i = $crawler->filterXPath('//div[@class="post-content"]/p//img')->image()->getUri();

		$response = $client->request('GET', $i);
		$img = $response->getBody();

		$cache->set('img', base64_encode($img), 10 * 60);

		return $img;
	}
}
